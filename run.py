#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import create_app, create_scheduler
from config import configs
import os
import sys

evn = os.environ.get('FLASK_ENV', 'default')

if not evn:
    raise Exception('Please input app environment')

app = create_app(config=configs[evn])
scheduler = create_scheduler(app)

scheduler.start()
app.run(host='0.0.0.0', port=app.config['APP_LISTEN_PORT'])

