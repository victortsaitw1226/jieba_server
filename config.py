import os
from celery.schedules import crontab   

class Dev:
    DEBUG = True
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    LOG_PATH = BASE_DIR + '/logs'
    APP_LISTEN_PORT = 8081
    SCHEDULER_API_ENABLED = True
    MONGO_HOST='10.60.10.128'
    MONGO_PORT=27017
    ES_HOST='10.60.10.128'
    ES_PORT=9200
    MYSQL_HOST = '10.60.10.128'
    MYSQL_PORT = 3306
    MYSQL_DB = 'peace'
    MYSQL_USER = 'root'
    MYSQL_PASSWORD = 'ibdo'
    JOBS = [
        {
            'id': 'job1',
            'func': 'app.jobs.word_cut:word_cut_job',
            'args': (),
            'trigger': 'interval',
            'seconds': 300
        },
        {
            'id': 'job2',
            'func': 'app.jobs.candidate:candidate_job',
            'args': (),
            'trigger': 'interval',
            'seconds': 300
        }
    ]

# ==========================================================================
class Production:
    DEBUG = False
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    LOG_PATH = BASE_DIR + '/logs'
    APP_LISTEN_PORT = 8081
    SCHEDULER_API_ENABLED = True
    MONGO_HOST='210.202.47.189'
    MONGO_PORT=27017
    MONGO_USER='ibdo'
    MONGO_PASSWORD='ibdo2018'
    ES_HOST='210.202.47.189'
    ES_PORT=9200
    MYSQL_HOST = '210.202.47.189'
    MYSQL_PORT = 3306
    MYSQL_DB = 'peace'
    MYSQL_USER = 'peace'
    MYSQL_PASSWORD = 'Foxconn@123'
    JOBS = [
        {
            'id': 'job1',
            'func': 'app.jobs.es_job:es_job',
            'args': (),
            'trigger': 'interval',
            'seconds': 300
        },
        {
            'id': 'job2',
            'func': 'app.jobs.mysql_job:mysql_job',
            'args': (),
            'trigger': 'interval',
            'seconds': 300
        }
        #{
        #    'id': 'job3',
        #    'func': 'app.jobs.score_job:score_job',
        #    'args': (),
        #    'trigger': 'interval',
        #    'seconds': 180
        #}
    ]

configs = {
  'dev' : Dev,
  'production' : Production,
  }

