FROM python:3.6
COPY . /app
WORKDIR /app
RUN apt-get update
RUN apt-get install -y vim
RUN pip install -r requirements.txt
ENV FLASK_ENV="dev"
CMD ["python3", "run.py"]
