#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify
from flask_apscheduler import APScheduler
from .utils import Log, Mongo, Elasticsearch, MySQL

def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)

    mongo = Mongo(host=app.config['MONGO_HOST'],
                  port=app.config['MONGO_PORT'],
                  user=app.config['MONGO_USER'],
                  password=app.config['MONGO_PASSWORD'])
    elasticsearch = Elasticsearch(host=app.config['ES_HOST'],
                                  port=app.config['ES_PORT'])

    mysql = MySQL(host=app.config['MYSQL_HOST'],
                  port=app.config['MYSQL_PORT'],
                  db = app.config['MYSQL_DB'],
                  user= app.config['MYSQL_USER'],
                  password = app.config['MYSQL_PASSWORD'])

    log = Log(name=__name__, path=app.config['LOG_PATH'])

    @app.errorhandler(404)
    def not_found(error):
        return jsonify(result=dict({
          'error': error
        }))

    @app.errorhandler(Exception)
    def catch_error(error):
        log.exception(error)
        return jsonify(result=dict({
          'error': error
        }))

    # Register Views
    from app.main.controllers import main
    app.register_blueprint(main)

    return app


def create_scheduler(app):
    scheduler = APScheduler()
    scheduler.init_app(app)
    return scheduler
