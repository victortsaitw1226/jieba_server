#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Blueprint, request, jsonify
from ..utils import log
from ..service import word

main = Blueprint('main', __name__, url_prefix='/')

@main.route('/status', methods=['GET'])
def status():
    return 'jieba-server:v0.1'


@main.route('/cut', methods=['POST'])
def cut():
    sentence = request.form.get("sentence", "")
    word_list = word.cut(sentence)
    return jsonify(result=dict({
        'result': 'success',
        'data': word_list
    }))

