#!/usr/bin/env python
# -*- coding: utf-8 -*-
from opencc import OpenCC
import jieba
from ..utils import log

def strQ2B(src):
    """full width to half width"""
    dst = ""
    for c in src:
        inside_code = ord(c)
        # full width space
        if inside_code == 12288:  
            inside_code = 32
        # full width symbols and characters
        elif (inside_code >= 65281 and inside_code <= 65374):
            inside_code -= 65248
        dst += chr(inside_code)
    return dst

def strB2Q(src):
    """half width to full width"""
    dst = ""
    for c in src:
        inside_code=ord(c)
        # half width space
        if inside_code == 32:
            inside_code = 12288
        # half width symbols and characters
        elif inside_code >= 32 and inside_code <= 126:
            inside_code += 65248
        dst += chr(inside_code)
    return dst

def t2s(sentence):
    """tranditional to simplified"""
    cc = OpenCC('t2s')
    return cc.convert(sentence)

def s2t(sentence):
    """simplified to traditional"""
    cc = OpenCC('s2t')
    return cc.convert(sentence)

def cut(sentence):
    log.debug('==sentence==')
    log.debug(sentence)
    # remove spaces
    #sentence = sentence.strip()
    # convert to lower case character
    sentence = sentence.lower()
    # convert to half width character
    sentence = strQ2B(sentence)
    # convert to simplified chinese
    sentence = t2s(sentence)
    words = []
    jieba.load_userdict('/app/app/service/extra_dict/add_words.txt')
    for word in jieba.cut(sentence):
        words.append(word)
    return words

if __name__ == '__main__':
    print(cut('我愛羅'))
    print(cut('自從郭宣布要選總統後 很多像韓國瑜對經濟、兩岸都不了解的草包網民和名嘴 突然都像專家一樣去嘲笑一個能和中美兩國領袖談論事情的跨國企業總裁了 都說韓國瑜只會說空話的草包，大部分的網民不也都這樣嗎'))
