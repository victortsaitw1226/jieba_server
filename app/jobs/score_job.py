# -*- coding: utf-8 -*-
import datetime
import pymongo
import requests
from ..utils import log, mongo, elasticsearch, mysql
from bson.objectid import ObjectId
from datetime import date

def insertToDocuments():
    log.info('====insertToMySQ====')
    #last_data = mysql.selectOne('SELECT * FROM document docid = "5d091b40b97fc5852b6d7150"')
    #last_source_id = last_data.get('docid', '')
    #log.info('last id:%s' % last_source_id)
    #print('last id:%s' % last_source_id)

    today = date.today().isoformat()
    # Loop all the data needed to insert to es
    cursor = mongo.find('history', 'data', {"PostDate": {'$regex': today + '.*'}})

    # Loop all the data needed to be cut in mongo
    for data in cursor:

        #neg_score = data.get('Nag_score', '0.0')
        neg_score = data['Nag_score']
        neg_score = float(neg_score)

        #pos_score = data.get('Pos_score', '0.0')
        pos_score = data['Pos_score']
        pos_score = float(pos_score)

        evaluation = 2
        if pos_score > neg_score:
            evaluation = 1
        elif pos_score < neg_score:
            evaluation = 0
        print('pos_score:%f, neg_score:%f, evaluation:%d' % (pos_score, neg_score, evaluation))
            

        browse = data.get('browse', '0')
        browse = int(browse)

        response = data.get('response', '0')
        response = int(response)

        source_type = data.get('sourceType', '')
        if source_type == 'news':
            source_type = 0
        elif source_type == 'soucial':
            source_type = 1
        else:
            source_type = 2

        post_date = data.get('PostDate', '')
        if post_date:
            post_date = post_date.split('.')[0]
            post_date = datetime.datetime.strptime(post_date, '%Y-%m-%dT%H:%M:%S')
        else:
            post_date = datetime.datetime.now()
 
        content = data.get('SourceContent', '')
        content = content[:10000]
        
        query = 'UPDATE document SET posscore=%s, negscore=%s, evaluation=%s, brocount=%s, repcount=%s  WHERE docid = %s'

        mysql.replace(query,
            (pos_score, neg_score, 
             evaluation, browse, response, str(data.get('_id', ''))))


def refresh_platfrom():
    url = "http://210.202.47.189:8080/ai-board/dataMgr.resetDocSum.ax"
    myResponse = requests.get(url)
    #print(myResponse)

def score_job():
    insertToDocuments()
    #insertToCandidate()
    refresh_platfrom()
