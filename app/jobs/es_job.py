# -*- coding: utf-8 -*-
import datetime
import traceback, sys
from ..utils import log, mongo, elasticsearch, mysql
from bson.objectid import ObjectId

def insertToES(data):
    source_id = data['_id']
    source_type = data.get('sourceType', '')
    if source_type == 'news':
        source_type = 0
    elif source_type == 'soucial':
        source_type = 1
    else:
        source_type = 2

    candidate = data.get('candidate', '')
    if candidate == 'WenJe_Ko':
        people = "柯文哲"

    elif candidate == 'ChingTe_Lai':
        people = "賴清德"

    elif candidate == 'IngWen_Tsai':
        people = "蔡英文"

    elif candidate == 'Terry_Gou':
        people = "郭台銘"

    elif candidate == 'Kuoyu_Han':
        people = "韓國瑜"

    create_date = data.get("CreateDate", data.get("createDate", datetime.datetime.now()))

    browse = data.get('browse', '0')
    response = data.get('response', '0')

    neg_score = data.get('Nag_score', '0.0')
    #neg_score = data.get('Nag_score', '0.0')
    neg_score = float(neg_score)

    pos_score = data.get('Pos_score', '0.0')
    #pos_score = data['Pos_score']
    pos_score = float(pos_score)

    evaluation = 2
    if pos_score > neg_score:
        evaluation = 1
    elif pos_score < neg_score:
        evaluation = 0
    print('pos_score:%f, neg_score:%f, evaluation:%d' % (pos_score, neg_score, evaluation))

    elasticsearch.upsert('crawl', 'data', str(source_id), {
        "data_id": data["data_id"],
        "postauthor": data["Author"],
        "Author_ID": data["Author_ID"],
        "createdate": create_date,
        "docid": data["DocID"],
        "MainID": data["MainID"],
        "docboard": data["sourceAreaName"],
        "content": data["SourceContent"],
        "media": data["sourceName"],
        "SourceType": data["sourceType"],
        "docurl": data["sourceUrl"],
        "PostDate": data["PostDate"],
        "contentType": data["contentType"],
        "candidate": people,
        "mediatype": source_type,
        "datafrom": source_type,
        "title": data["Title"],
        "brocount": int(browse),
        "repcount": int(response),
        "datatype": 0,
        "posscore": pos_score,
        "negscore": neg_score,
        "evaluation": evaluation,
        "join_field": {
            "name": "document"
        }
    })
    elasticsearch.insert('crawl', 'data', {
        "candidate": people,
        "join_field": {
            "name": "candidate", 
            "parent": str(source_id)
        }
    })


def es_job():
    print('======insertToES======')
    last_data = elasticsearch.find_last_one('crawl')
    last_source_id = last_data['_id']
    print('es last id:%s' % last_source_id)
    # Loop all the data needed to insert to es
    if last_source_id:
        cursor = mongo.find_greater_than('history', 'data', '_id', ObjectId(last_source_id))
    else:
        cursor = mongo.find_all('history', 'data')

    for data in cursor:
        try:
            insertToES(data)
        except:
            traceback.print_exc(file=sys.stdout)
            print(data)
