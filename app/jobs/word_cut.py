# -*- coding: utf-8 -*-
import datetime
from ..utils import log, mongo, elasticsearch, mysql
from ..service import word
from bson.objectid import ObjectId


def word_cut_job():
    log.debug(datetime.datetime.now())
    
    # find the last one that has been cut in mongo
    last_data = mongo.find_last_one('crawl', 'word_cut')

    if last_data:
        last_source_id = last_data.get('source_id', '')
        cursor = mongo.find_greater_than('crawl', 'data', '_id', ObjectId(last_source_id))
    else:
        cursor = mongo.find_all('crawl', 'data')

    # Loop all the data needed to be cut in mongo
    for data in cursor:
        # cut the sentence
        _id = data['_id']
        content = data['SourceContent']
        word_list = word.cut(content)

        # insert to the word_cut collection in mongo
        mongo.upsert('crawl', 'word_cut', 
                dict({'source_id': _id
        }),
        dict({
            'source_id': _id,
            'words': word_list
        }))
