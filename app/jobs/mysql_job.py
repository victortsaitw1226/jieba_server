# -*- coding: utf-8 -*-
import datetime
import pymongo
import requests
from ..utils import log, mongo, elasticsearch, mysql
from bson.objectid import ObjectId

def insertToDocuments():
    log.info('====insertToMySQ====')
    last_data = mysql.selectOne('SELECT * FROM document ORDER BY docid DESC LIMIT 1')
    last_source_id = last_data.get('docid', '')
    log.info('last id:%s' % last_source_id)
    print('mysql last id:%s' % last_source_id)
   
    # Loop all the data needed to insert to es
    if last_source_id:
        cursor = mongo.find_greater_than('history', 'data', '_id', ObjectId(last_source_id))
    else:
        cursor = mongo.find_all('history', 'data')

    # Loop all the data needed to be cut in mongo
    for data in cursor:

        neg_score = data.get('Nag_score', '0.0')
        #neg_score = data.get('Nag_score', '0.0')
        neg_score = float(neg_score)

        pos_score = data.get('Pos_score', '0.0')
        #pos_score = data['Pos_score']
        pos_score = float(pos_score)

        evaluation = 2
        if pos_score > neg_score:
            evaluation = 1
        elif pos_score < neg_score:
            evaluation = 0
        print('pos_score:%f, neg_score:%f, evaluation:%d' % (pos_score, neg_score, evaluation))
            

        browse = data.get('browse', '0')
        browse = int(browse)

        response = data.get('response', '0')
        response = int(response)

        source_type = data.get('sourceType', '').strip()
        source = 'quickseek'
        data_type = 0
        if source_type == 'news':
            source_type = 0
        elif source_type == '3':
            source_type = 3
            source = data.get('sourceName', '')
            data_type = 2
        elif source_type == '4':
            source_type = 4
            source = data.get('sourceName', '')
            data_type = 3
        else:
            source_type = 2

        post_date = data.get('PostDate', '')
        if post_date:
            post_date = post_date.split('.')[0]
            post_date = datetime.datetime.strptime(post_date, '%Y-%m-%dT%H:%M:%S')
        else:
            post_date = datetime.datetime.now()
 
        content = data.get('SourceContent', '')
        content = content[:10000]
        
        query = 'INSERT INTO document(' + \
                'docid, createdate, source, mediatype, media, ' + \
                'doccategory, docboard, postdate, postauthor, ' + \
                'title, content, docurl, posscore, ' + \
                'negscore, evaluation, brocount, repcount, ' + \
                'isvalid, postdate_date) VALUES(%s, %s, %s, %s, %s, ' + \
                '%s, %s, %s, %s, ' + \
                '%s, %s, %s, %s, ' + \
                '%s, %s, %s, %s, %s, %s) ' + \
                'on duplicate key update ' + \
                'title=values(title), ' + \
                'content=values(content), ' + \
                'posscore=values(posscore), ' + \
                'negscore=values(negscore), ' + \
                'evaluation=values(evaluation), ' + \
                'brocount=values(brocount), ' + \
                'repcount=values(repcount)' 

        mysql.replace(query,
            (str(data.get('_id', '')),
            datetime.datetime.now(),
            source,
            source_type,
            data.get('sourceName', ''),
            '',
            data.get('SourceAreaName', ''),
            post_date,
            data.get('Author', ''),
            data.get('Title', ''),
            content,
            data.get('sourceUrl', ''),
            pos_score, neg_score, 
            evaluation, browse, response, 1,
            post_date.date()))

        candidate = data.get('candidate', '')
        if candidate == 'WenJe_Ko' or candidate == "柯文哲":
            people = "柯文哲"

        elif candidate == 'ChingTe_Lai' or candidate == "賴清德":
            people = "賴清德"

        elif candidate == 'IngWen_Tsai' or candidate == "蔡英文":
            people = "蔡英文"

        elif candidate == 'Terry_Gou' or candidate == "郭台銘":
            people = "郭台銘"

        elif candidate == 'Kuoyu_Han' or candidate == "韓國瑜":
            people = "韓國瑜"

        query = 'INSERT IGNORE INTO candidate(' + \
                    'docid, candidate) VALUES(%s, %s)'
        try:
            mysql.insert(query, (str(data['_id']), people))
        except pymongo.errors.DuplicateKeyError as e:
            pass

def insertToCandidate():
    log.debug(datetime.datetime.now())

    related_words = dict({
        "郭台銘": ["郭台銘", "鴻海", "富士康", "郭董", "果凍"],
        "韓國瑜": ["韓國瑜", "高雄市長", "草包", "韓總", "韓導", "韓粉"],
        "柯文哲": ["柯文哲", "柯P", "白色力量", "台北市長", "嗡嗡嗡", "柯屁", "漩渦老人", "一日幕僚"],
        "蔡英文": ["蔡英文", "辣台妹", "小英", "總統", "空心菜", "菜英文"],
        "王金平": ["王金平", "王院長", "公道伯"],
        "朱立倫": ["朱立倫", "朱市長", "做好做滿"],
        "賴清德": ["賴清德", "賴院長", "賴市長", "賴功德", "賴神", "仁醫"]
    })

    for people in related_words.keys():
        datas = elasticsearch.find_data_by_keyword_in_one_day('crawl', related_words[people])
        for data in datas:
            doc_id = data['_id']

            query = 'INSERT IGNORE INTO candidate(' + \
                    'docid, candidate) VALUES(%s, %s)'
            try:
                mysql.insert(query, (doc_id, people))
            except pymongo.errors.DuplicateKeyError as e:
                pass

def refresh_platfrom():
    url = "http://210.202.47.189:8080/ai-board/dataMgr.resetDocSum.ax"
    myResponse = requests.get(url)
    #print(myResponse)

def mysql_job():
    insertToDocuments()
    #insertToCandidate()
    refresh_platfrom()
