#!/usr/bin/env python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch

class ElasticsearchAgent:
    class __ElasticsearchAgent:
        def __init__(self, host, port):

            self.es = Elasticsearch([{'host': host, 'port': port}])

            if self.es.ping():
                print('Elasticsearch connected')
            else:
                raise Error('Elasticsearch disconnected')
         
        def insert(self, index, doc_type, data):
            self.es.index(index=index, doc_type=doc_type, body=data, routing=1)

        def upsert(self, index, doc_type, data_id, data):
            doc = dict({
                'doc_as_upsert': True,
                'doc': data
            })
            return self.es.update(index=index, doc_type=doc_type, id=data_id, body=doc)

        def find_last_one(self, index):
            if not self.es.indices.exists(index=index):
                return {'_id': ''}

            d = self.es.search(index=index, body={
                "query": {
                    "exists": {"field": "data_id"}
                }, 
                "size": 1, 
                "sort": [{"_id":{"order":"desc"}}]
            })
            data = d['hits']['hits']
            if len(data) == 0:
                return {'_id': ''}
            return data[0]

        def find_data_by_keyword_in_one_day(self, index, keywords):
            keywords_size = len(keywords)
            str_keywords = '"' + keywords.pop() + '"'
            for keyword in keywords:
                str_keywords = str_keywords + ' OR "' + keyword + '"'
            print(str_keywords)
            d = self.es.search(index=index, size=10000, body={
                "query": {
                    "bool": {
                        "must": [
                            {
                                "query_string": {
                                    "default_field": "SourceContent",
                                    "query": str_keywords
                                } 
                            },
                            {
                                "range": {
                                    "PostDate": {
                                        "gte": "now-30d",
                                        "lte": "now"
                                    }
                                }
                            }
                        ]
                    }
                }
            })
            datas = d['hits']['hits']
            return datas


    instance = None
    def __init__(self, host, port):

        if not ElasticsearchAgent.instance:
            ElasticsearchAgent.instance = ElasticsearchAgent.__ElasticsearchAgent(host, port)

    def __getattr__(self, name):
        return getattr(self.instance, name)



if __name__ == '__main__':
    m = ElasticsearchAgent('210.202.47.189', 9200)
    related_words = dict({
        "郭台銘": ["郭台銘", "鴻海", "富士康", "郭董", "果凍"],
        "韓國瑜": ["韓國瑜", "高雄市長", "草包", "韓總", "韓導", "韓粉"],
        "柯文哲": ["柯文哲", "柯P", "白色力量", "台北市長", "嗡嗡嗡"],
        "蔡英文": ["蔡英文", "辣台妹", "小英", "總統", "空心菜", "菜英文"],
        "王金平": ["王金平", "王院長", "公道伯"],
        "朱立倫": ["朱立倫", "朱市長", "做好做滿"],
        "賴清德": ["賴清德", "賴院長", "賴市長", "賴功德", "賴神", "仁醫"]
    })

    for people in related_words.keys():
        datas = m.find_data_by_keyword_in_one_day('crawl', related_words[people])
        print('%s: %d' % (people, len(datas)))
