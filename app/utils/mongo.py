#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymongo
from bson.objectid import ObjectId

class MongoAgent:
    class __MongoAgent:
        def __init__(self, host, port, user, password):
            self.client = pymongo.MongoClient(host, port, username=user, password=password)

        def getDB(self, db):
            return self.client[db]

        def getCollection(self, db, collection):
            return self.client[db][collection]


        def find(self, db, collection, query):
            collection = self.getCollection(db, collection)
            cursor = collection.find(query)
            return cursor
        
        def find_last_one(self, db, collection):
            collection = self.getCollection(db, collection)
            return collection.find_one({}, sort=[('_id',pymongo.DESCENDING)])

        def find_greater_than_id(self, db, collection, _id):
            collection = self.getCollection(db, collection)
            cursor = collection.find({'_id': {'$gt': ObjectId(_id)}})
            return cursor

        def find_greater_than(self, db, collection, col, value):
            collection = self.getCollection(db, collection)
            cursor = collection.find({col: {'$gt': value}})
            return cursor

        def find_all(self, db, collection):
            collection = self.getCollection(db, collection)
            cursor = collection.find({})
            return cursor

        def insert(self, db, collection, data):
            collection = self.getCollection(db, collection)
            _id = collection.insert_one(data).inserted_id
            return _id

        def upsert(self, db, collection, query, data):
            collection = self.getCollection(db, collection)
            return collection.update(query, {'$set': data}, upsert=True)


    instance = None
    def __init__(self, host, port, user, password):

        if not MongoAgent.instance:
            MongoAgent.instance = MongoAgent.__MongoAgent(host, port, user, password)

    def __getattr__(self, name):
        return getattr(self.instance, name)



if __name__ == '__main__':
    m = MongoAgent('210.202.47.189', 27017, 'ibdo', 'ibdo2018')
    i = 0
    for d in m.find_all('crawl', 'data'):
        i = i + 1
    print(i)
