#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pymysql
import pymysql.cursors

class MySQLAgent:
    class __MySQLAgent:
        def __init__(self, **kwargs):
            host = kwargs.get('host', 'localhost')
            port = kwargs.get('port', 3306)
            username = kwargs.get('user','')
            password = kwargs.get('password', '')
            database = kwargs.get('db', '')
            self.db = pymysql.connect(
                           host=host,
                           port=port,
                           user=username,
                           password=password,
                           db=database,
                           charset='utf8mb4')

    def insert(self, query, parameters=()):
        with self.db.cursor() as cursor:
            cursor.execute(query, parameters)
        self.db.commit()

    def replace(self, query, parameters=()):
        print(query)
        print(parameters)
        with self.db.cursor() as cursor:
            cursor.execute(query, parameters)
        self.db.commit()

    def selectOne(self, query, parameters=()):
        result = None
        with self.db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(query, parameters)
            result = cursor.fetchone()

        if not result:
            return {}
        return result

    def selectAll(self, query, parameters=()):
        result = None
        with self.db.cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(query, parameters)
            result = cursor.fetchall()
        return result

    instance = None
    def __init__(self, **kwargs):

        if not MySQLAgent.instance:
            MySQLAgent.instance = MySQLAgent.__MySQLAgent(**kwargs)

    def __getattr__(self, name):
        return getattr(self.instance, name)



if __name__ == '__main__':
    m = MySQLAgent(host='10.60.10.128', port=3306, db='peace', user='root', password='ibdo')
    r = m.selectAll('select * from document limit 1', ())
    print(r)
