from .logger import Log as Logger
log = None
def Log(**kwargs):
    global log
    print('init logger')
    log = Logger(**kwargs)
    return log



from .mongo import MongoAgent
mongo= None
def Mongo(**kwargs):
    global mongo
    mongo = MongoAgent(**kwargs)
    return mongo


from .es import ElasticsearchAgent
elasticsearch = None
def Elasticsearch(**kwargs):
    global elasticsearch
    elasticsearch = ElasticsearchAgent(**kwargs)
    return elasticsearch 

from .mysql import MySQLAgent
mysql = None
def MySQL(**kwargs):
    global mysql 
    mysql = MySQLAgent(**kwargs)
    return mysql

