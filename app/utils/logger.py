import logging
import time
from logging.handlers import TimedRotatingFileHandler
import os
import traceback
import sys
import platform

class Log:
    class __Log:
        def __init__(self, name, path):
            # self.val = arg
            host_name = platform.node()
            path = os.path.join(path, host_name)

            self.checkDir(path)

            self.logger = logging.getLogger(name)
            self.logger.setLevel(logging.DEBUG)
            self.formater = logging.Formatter(
              '%(asctime)s.%(msecs)03d[%(name)s:%(process)d]%(levelname)s:%(message)s',
              '%Y-%m-%d %H:%M:%S'
            )
            self.when = "midnight"
            #self.when = "m"
            self.interval=1
            self.backupCount=14
            self.suffix = '%Y%m%d'
            self.path = path
            self.logger.addHandler(self.setDebug())
            self.logger.addHandler(self.setInfo())
            self.logger.addHandler(self.setWarn())
            self.logger.addHandler(self.setError())
            self.changeLevelFormat()

        def changeLevelFormat(self):
            logging.addLevelName(logging.DEBUG, 'debug');
            logging.addLevelName(logging.INFO, 'info');
            logging.addLevelName(logging.WARN, 'warn');
            logging.addLevelName(logging.ERROR, 'error');
           

        def checkDir(self, dirName):
            # Create target Directory if don't exist
            if not os.path.exists(dirName):
                os.makedirs(dirName)
                print("Directory " , dirName ,  " Created ")
            else:    
                print("Directory " , dirName ,  " already exists")

        def setDebug(self):            
            path = os.path.join(self.path, 'debug.log')
            handler = TimedRotatingFileHandler(path,
                                               when=self.when,
                                               interval=self.interval,
                                               backupCount=self.backupCount)
            handler.suffix = self.suffix
            handler.setFormatter(self.formater)
            handler.setLevel(logging.DEBUG)
            return handler

        def setInfo(self):            
            path = os.path.join(self.path, 'info.log')
            handler = TimedRotatingFileHandler(path,
                                               when=self.when,
                                               interval=self.interval,
                                               backupCount=self.backupCount)
            handler.suffix = self.suffix
            handler.setFormatter(self.formater)
            handler.setLevel(logging.INFO)
            return handler

        def setWarn(self):            
            path = os.path.join(self.path, 'warn.log')
            handler = TimedRotatingFileHandler(path,
                                               when=self.when,
                                               interval=self.interval,
                                               backupCount=self.backupCount)
            handler.suffix = self.suffix
            handler.setFormatter(self.formater)
            handler.setLevel(logging.WARN)
            return handler

        def setError(self):            
            path = os.path.join(self.path, 'error.log')
            handler = TimedRotatingFileHandler(path,
                                               when=self.when,
                                               interval=self.interval,
                                               backupCount=self.backupCount)
            handler.suffix = self.suffix
            handler.setFormatter(self.formater)
            handler.setLevel(logging.ERROR)
            return handler

        def extract_function_name(self):
            tb = sys.exc_info()[-1]
            stk = traceback.extract_tb(tb, 1)
            fname = stk[0][3]
            return fname

        def exception(self, e):
            self.logger.error(
                "Function {function_name} raised {exception_class} ({exception_docstring}): {exception_message}".format(
                    function_name = self.extract_function_name(), #this is optional
                    exception_class = e.__class__,
                    exception_docstring = e.__doc__,
                    exception_message = str(e)))

        def debug(self, msg):
            self.logger.debug(msg)

        def info(self, msg):
            self.logger.info(msg)

        def warn(self, msg):
            self.logger.warn(msg)

        def error(self, msg):
            self.logger.error(msg)

        def run(self):
            for i in range(10):
                self.logger.info("This is a info test!" + str(i))
                self.logger.debug("This is a debug test!" + str(i))
                self.logger.warn("This is a warn test!" + str(i))
                self.logger.error("This is a error test!" + str(i))
                time.sleep(7)

        def __str__(self):
            return repr(self) + self.val

    instance = None
    def __init__(self, name='', path=''):
        if not Log.instance:
            Log.instance = Log.__Log(name, path)
        #else:
        #    Log.instance.val = arg

    def __getattr__(self, name):
        return getattr(self.instance, name)


#----------------------------------------------------------------------
if __name__ == "__main__":
    log = Log('myApp', 'logs')
    try:
        a = 1 / 0
    except Exception as e:
        log.exception(e)

